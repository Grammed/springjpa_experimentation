package fun.madeby;

import fun.madeby.models.Beer;
import fun.madeby.models.Brewer;
import fun.madeby.models.Category;
import fun.madeby.repositories.*;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import static fun.madeby.utilities.ConsoleInputTool.*;
import static fun.madeby.utilities.ConsolePrintTool.*;



@SpringBootApplication
public class BeerApp {
ConfigurableApplicationContext ctx;
BeerRepositoryImpl beerRepositoryImpl;
BrewerRepositoryImpl brewerRepositoryImpl;
CategoryRepositoryImpl categoryRepositoryImpl;



public static void main(String[] args) {
	BeerApp app = new BeerApp();
	app.ctx = SpringApplication.run(BeerApp.class);
	app.beerRepositoryImpl = app.ctx.getBean(BeerRepositoryImpl.class);
	app.brewerRepositoryImpl = app.ctx.getBean(BrewerRepositoryImpl.class);
	app.categoryRepositoryImpl = app.ctx.getBean(CategoryRepositoryImpl.class);
	Beer beer = app.beerRepositoryImpl.findById(4);
	System.out.println(beer.toString());
	beer.setName("WasAOC");
	app.beerRepositoryImpl.update(beer);
	System.out.println(app.beerRepositoryImpl.findById(4).toString());
	System.out.println(app.brewerRepositoryImpl.findById(1).toString());
//	System.out.println(app.categoryRepositoryImpl.findById(2).toString());
	Brewer brewer = app.brewerRepositoryImpl.findById(1);
	Category category = app.categoryRepositoryImpl.findById(2);
	System.out.println("-------------------------------");


	Beer beerPersist = new Beer("Kumquat ale", brewer, category, 12.0, 1000, 8.8f);
	System.out.println(beerPersist);
	long id = app.beerRepositoryImpl.save(beerPersist);
	System.out.println("Beer " + id + " has been saved");
	//System.out.println(beerRepositoryImpl.findById(beerRepositoryImpl.save(beerPersist)).toString());
	//Collection<Beer> beerCollection3_8 = app.beerRepositoryImpl.findAllAtAbv(4f);
	//System.out.println(beerCollection3_8.stream().map(Beer::getName).collect(Collectors.toList()));*/

	app.menuOne();
}

private void menuOne() {
	while (true) {
		printTitle("Welcome to The Beer Department", '_');

		System.out.println("1. Print All Beers\n" +
			                   "2. Add/delete a beer\n" +
			                   "3. Print All Brewers\n" +
			                   "4. Add/delete a Brewer\n" +
			                   "5. Print All Orders\n" +
			                   "6. Add/delete an order\n" +
			                   "7. Order related something01\n" +
			                   "8. Order related something02\n" +
			                   "0. exit");
		int choice = askUserInteger("Your choice: ", 0, 8);
		switch (choice) {
			case 1:
				printTitle("All beers in database:");
				Collection<Beer> allBeers = beerRepositoryImpl.findAll();
				System.out.println("Streamed");
				System.out.println(allBeers.stream().map((Beer::getName)).collect(Collectors.toList()));
				List<Beer> beersList = (List<Beer>) allBeers;
				for(Beer b: beersList)
					System.out.println(b.getId() + " " + b.getName());
				break;
			case 2:
				System.out.println("Add/delete beers");
				break;
			case 3:
				System.out.println("Print all brewers");
				break;
			case 4:
				System.out.println("Add/delete brewers");
				break;
			case 5:
				System.out.println("Print all orders");
				break;
			case 6:
				System.out.println("Add/delete orders");
				break;
			case 7:
				System.out.println("Order related something01");
				break;
			case 8:
				System.out.println("Order related something02");
				break;
			case 0:
				break;
			default:
				System.out.println("PLease enter a valid option.");
		}

	}
}
}
