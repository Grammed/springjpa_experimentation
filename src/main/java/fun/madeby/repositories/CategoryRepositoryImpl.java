package fun.madeby.repositories;

import fun.madeby.models.Category;
import org.springframework.stereotype.Repository;
import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
private EntityManagerFactory emf;
private EntityManager em;

public EntityManager getEntityManager() {
	return em;
}

public void setEntityManager(EntityManager em) {
	this.em = em;
}

public EntityManagerFactory getEmf(){
	return emf;
}

@PersistenceUnit
public void setEmf(EntityManagerFactory emf){
	this.emf = emf;
	this.em = emf.createEntityManager();
}



@Override
public Category findById(int id) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	Category category = em.find(Category.class, id);
	//beer.getBrewer().getBeers().size();
	//beer.getCategory().getBeers().size();
	transaction.commit();
	return category;
}

@Override
public void delete(Category category) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	em.remove(category);
	transaction.commit();
}

@Override
public long save(Category category) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	em.persist(category);
	transaction.commit();
	return category.getId();
}

@Override
public void update(Category category) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	category = em.merge(category);
	em.getTransaction().commit();
	em.detach(category);
}

@Override
public void delete(int id) {
	Category category = findById(id);
	delete(category);
}

@Override
public Collection<Category> findAll() {
	EntityTransaction entityTransaction = em.getTransaction();
	entityTransaction.begin();
	TypedQuery<Category> typedQuery = em.createQuery("SELECT c from Category c", Category.class);
	List<Category> brewer = typedQuery.getResultList();
	entityTransaction.commit();
	return brewer;
}
}