package fun.madeby.repositories;

import fun.madeby.models.Brewer;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Repository
public class BrewerRepositoryImpl implements BrewerRepository {
private EntityManagerFactory emf;
private EntityManager em;

public EntityManager getEntityManager() {
	return em;
}

public void setEntityManager(EntityManager em) {
	this.em = em;
}

public EntityManagerFactory getEmf(){
	return emf;
}

@PersistenceUnit
public void setEmf(EntityManagerFactory emf){
	this.emf = emf;
	this.em = emf.createEntityManager();
}



@Override
public Brewer findById(long id) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	Brewer brewer = em.find(Brewer.class, id);
	//beer.getBrewer().getBeers().size();
	//beer.getCategory().getBeers().size();
	transaction.commit();
	return brewer;
}

@Override
public void delete(Brewer brewer) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	em.remove(brewer);
	transaction.commit();
}

@Override
public long save(Brewer brewer) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	em.persist(brewer);
	transaction.commit();
	return brewer.getId();
}

@Override
public void update(Brewer brewer) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	brewer = em.merge(brewer);
	em.getTransaction().commit();
	em.detach(brewer);
}

@Override
public void delete(long id) {
	Brewer brewer = findById(id);
	delete(brewer);
}

@Override
public Collection<Brewer> findAll() {
	EntityTransaction entityTransaction = em.getTransaction();
	entityTransaction.begin();
	TypedQuery<Brewer> typedQuery = em.createQuery("SELECT b from Brewer b", Brewer.class);
	List<Brewer> brewer = typedQuery.getResultList();
	entityTransaction.commit();
	return brewer;
}




}
