package fun.madeby.repositories;

import fun.madeby.models.Beer;
import fun.madeby.models.Brewer;

import java.util.Collection;

public interface BeerRepository {

Collection<Beer> findAllByBrewer(Brewer brewer);
void delete(Beer beer);
void delete(long id);
Collection<Beer> findAll();
Collection<Beer> findAllAtAbv(float alcohol);
Beer findById(long id);
long save(Beer beer);
void update(Beer beer);
void update(Beer original, Beer replacement);

}
