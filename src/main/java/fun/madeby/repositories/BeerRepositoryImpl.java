package fun.madeby.repositories;

import fun.madeby.models.Beer;
import fun.madeby.models.Brewer;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
private EntityManagerFactory emf;
private EntityManager em;

public EntityManager getEntityManager() {
	return em;
}

public void setEntityManager(EntityManager em) {
	this.em = em;
}

public EntityManagerFactory getEmf(){
	return emf;
}

@PersistenceUnit
public void setEmf(EntityManagerFactory emf){
	this.emf = emf;
	this.em = emf.createEntityManager();
}


@Override
public Collection<Beer> findAllByBrewer(Brewer brewer) {

	EntityTransaction entityTransaction = em.getTransaction();
	entityTransaction.begin();

	TypedQuery<Beer> typedQuery = em.createQuery("SELECT b from Beer b where b.brewer=:brewer", Beer.class);
	typedQuery.setParameter("brewer", brewer);
	List<Beer> beers = typedQuery.getResultList();

	entityTransaction.commit();
	return beers;
}

@Override
public Collection<Beer> findAllAtAbv(float alcohol) {

		EntityTransaction entityTransaction = em.getTransaction();
		entityTransaction.begin();

		TypedQuery<Beer> typedQuery = em.createQuery("SELECT b from Beer b where b.alcohol=:alcohol", Beer.class);
		typedQuery.setParameter("alcohol", alcohol);
		List<Beer> beers = typedQuery.getResultList();

		entityTransaction.commit();
		return beers;
}

@Override
public Beer findById(long id) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	Beer beer = em.find(Beer.class, id);
	//beer.getBrewer().getBeers().size();
	//beer.getCategory().getBeers().size();
	transaction.commit();
	return beer;
}

@Override
public void delete(Beer beer) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	em.remove(beer);
	transaction.commit();
}

@Override
public long save(Beer beer) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	//beer.setBrewer(beer.getBrewer()); //This was added when had
	//beer.setCategory(beer.getCategory());
	em.persist(beer);
	transaction.commit();
	return beer.getId();
}

@Override
public void update(Beer beer) {
	//
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	beer = em.merge(beer);
	em.getTransaction().commit();
	em.detach(beer);
}

@Override
public void delete(long id) {
	Beer beer = findById(id);
	delete(beer);
}

@Override
public Collection<Beer> findAll() {
	EntityTransaction entityTransaction = em.getTransaction();

	entityTransaction.begin();

	TypedQuery<Beer> typedQuery = em.createQuery("SELECT b from Beer b", Beer.class);
	List<Beer> beers = typedQuery.getResultList();

	entityTransaction.commit();

	return beers;
}


// Highly unlikely to ever be useful original would be retrieved then updated then normal update
// could be used this is just a plaything really then...delete.
@Override
public void update(Beer original, Beer replacement) {

	EntityTransaction entityTransaction = em.getTransaction();
	entityTransaction.begin();
	original = em.merge(replacement);
	em.getTransaction().commit();


}

}
