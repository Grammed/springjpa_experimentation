package fun.madeby.repositories;

import fun.madeby.models.Brewer;
import java.util.Collection;

public interface BrewerRepository {
void delete(Brewer brewer);
void delete(long id);
Collection<Brewer> findAll();
Brewer findById(long id);
long save(Brewer brewer);
void update(Brewer brewer);

}
