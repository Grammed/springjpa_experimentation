package fun.madeby.repositories;

import fun.madeby.models.Category;

import java.util.Collection;

public interface CategoryRepository {
void delete(Category category);
void delete(int id);
Collection<Category> findAll();
Category findById(int id);
long save(Category category);
void update(Category category);

}
