package fun.madeby.models;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Beers")
public class Beer {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "Id", nullable = false, unique = true)
private long id;
@Version
@Column(name = "Version")
private int version;
@Column(name = "Name", length = 50)
private String name;
@ManyToOne //no CASCADING here
@JoinColumn(name = "BrewerId")
private Brewer brewer;
@ManyToOne //no CASCADING
//@Column(name = "CategoryId", nullable = false, columnDefinition = "int default 0")
@JoinColumn(name = "CategoryId")
private Category category;
@Column(name = "Price", precision = 2, nullable = false, columnDefinition = "double default 0")
private double price;
@Column(name = "Stock", nullable = false, columnDefinition = "int default 0")
private int stock;
@Column(name = "Alcohol", precision = 1, nullable = false, columnDefinition = "float default 0")
private float alcohol;

public Beer() {}

public Beer(String name, Brewer brewer, Category category, double price, int stock, float alcohol) {
	this.name = name;
	this.brewer = brewer;
	this.category = category;
	this.price = price;
	this.stock = stock;
	this.alcohol = alcohol;
}

public Beer(long id, int version, String name, Brewer brewer, Category category, double price, int stock, float alcohol) {
	this.id = id;
	this.version = version;
	this.name = name;
	this.brewer = brewer;
	this.category = category;
	this.price = price;
	this.stock = stock;
	this.alcohol = alcohol;
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public int getVersion() {
	return version;
}

public void setVersion(int version) {
	this.version = version;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public Brewer getBrewer() {
	return brewer;
}

public void setBrewer(Brewer brewer) {
	this.brewer = brewer;
}

public Category getCategory() {
	return category;
}

public void setCategory(Category category) {
	this.category = category;
}

public double getPrice() {
	return price;
}

public void setPrice(double price) {
	this.price = price;
}

public int getStock() {
	return stock;
}

public void setStock(int stock) {
	this.stock = stock;
}

public float getAlcohol() {
	return alcohol;
}

public void setAlcohol(float alcohol) {
	this.alcohol = alcohol;
}

@Override
public String toString() {
	return "Beer{" +
		       "id=" + id +
		       ", version=" + version +
		       ", name='" + name + '\'' +
		       ", brewer=" + brewer +
		       ", category=" + category +
		       ", price=" + price +
		       ", stock=" + stock +
		       ", alcohol=" + alcohol +
		       '}';
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	Beer beer = (Beer) o;
	return id == beer.id;
}

@Override
public int hashCode() {
	return Objects.hash(id);
}
}
